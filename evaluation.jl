
using RidePooling
RP = RidePooling

using RidePooling_eval
ev = RidePooling_eval
using DataFrames
using Measurements

import Base.Filesystem
FS = Base.Filesystem

request_type = "drive_and_wait"
#request_type = "resubmission"
start = 800
paths = ["/scratch01.local/rhaag/comparison/$request_type/combined_delays/anywt1_anydt01/"],
    "/scratch01.local/rhaag/comparison/$request_type/combined_delays/anywt035_anydt01/"]
for path in paths
    model_path = path * "/models/"
	println("Path: $model_path")
    # Values I want to calculate for every model
    req_quantitys_mean = [:relative_delay,
        :delay,
        :relative_waiting_time,
        :waiting_time,
        :served,
        :requested_distance,
        :requested_time,
        :total_time,
        :travelling_time,
        :resubmitted_served_percentage,
        :dt_earliest_served,
        :dt_earliest,
        :relative_dt_earliest,
        :delay
    ]

    req_quantitys_sum = [
        :count_req,
        :resubmitted_all,
        :resubmitted_served,
    ]

    bus_quantitys = [
        :cooldown_time,
    ]

    model_quantitys = [
        :driven_distance,
        :efficiency,
        :quality
    ]


    ########## This is just, because the Data Frame constructor still needs unintuitive workarounds 😠
    rows = [:index => Int[],
        :dt_earliest_pickup => Float64[],
        :idle_weight => Float64[],
        :resubmission_time_factor => Float64[],
        :ν => Float64[],
        :t0 => Float64[],
        :request_type => Symbol[]
    ]

    for quan in req_quantitys_mean
        push!(rows, quan => Measurement[])
    end

    for quan in req_quantitys_sum
        push!(rows, quan => Float64[])
    end

    for quan in bus_quantitys
        push!(rows, quan => Measurement[])
    end

    for quan in model_quantitys
        push!(rows, quan => Measurement[])
    end

    rows = NamedTuple(rows)
    results = DataFrame(rows)

    ###########################################################
    filenames = readdir(model_path, join=false) # get Filenames
	filter!(x->occursin(".model", x), filenames)
    # Evalkuate Models
    for model_nr in filenames
        model = loadmodel(model_path * String(model_nr))
        row = Dict()

        row[:index] = parse(Int, FS.splitext(model_nr)[1])
        print(model.request_type)
        row[:dt_earliest_pickup] = (model.request_type != :now) ? model.random_gens[model.request_type][2] : 0
        println("\t\t$(row[:dt_earliest_pickup])")
        row[:idle_weight] = model.cost_idle_weight
        row[:ν] = model.ν
        row[:t0] = model.t0
        row[:request_type] = model.request_type
        row[:resubmission_time_factor] = model.resubmission_time_factor
        for quan in req_quantitys_mean
            row[quan] = ev.quantity(quan, model.requests[start:end])
        end
        for quan in req_quantitys_sum
            row[quan] = ev.sum(quan, model.requests[start:end])
        end
        for quan in bus_quantitys
            row[quan] = ev.quantity(quan, model.agents, model)
        end
        for quan in model_quantitys
            f = @eval (ev.$quan)
            row[quan] = f(model)
        end
        using Serialization
        push!(results, row)
    end

    println("Saving as $path results.df")
    using Serialization
    serialize(path * "results.df", results)

end
