
route_matrix_path = "route_matrices/"

mapToName = Dict([
    (:star_grid_map,(32,32)) => "stargrid_32_32",
])

#Speeds needed for t0 -> 1
mapToSpeed = Dict([
    (:star_grid_map,(32,32)) => Dict(1=>3.6),
])

mapTo_t0 =  Dict([
    (:star_grid_map,(32,32)) => 31.179049489898734,
])

mapToMatrix = Dict([map => route_matrix_path*name*"/" for (map, name) in mapToName])
