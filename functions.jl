function getX(index, xmin, xmax, xlen, ylen)
	xstep = (xmax-xmin)/xlen
	return  (trunc(Int64, index/ylen)) * xstep + xmin #Ylen belongs here.
end

function getY(index, ymin, ymax, ylen)
	ystep = (ymax-ymin)/ylen
	return (index%ylen) * ystep + ymin
end

function rand_value(lower,upper)
    rand(Float64) *(upper-lower) + lower
end