using CSV
using DataFrames
using DelimitedFiles


path = ARGS[1]
xname = "frequency"
yname = "dt_earliest_pickup"




csv_filenames = readdir(path, join=true)

df = DataFrame(CSV.File(csv_filenames[1]))


for name in csv_filenames[2:end]
    temp = DataFrame(CSV.File(name))
    append!(df, temp)
end
println("Saving Combined Dataframe to $path")

CSV.write(path*"/results.csv", df)

