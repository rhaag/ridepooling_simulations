using Pkg
Pkg.activate(".")

#Pkg.add(url="https://gitlab.gwdg.de/smuehle1/RidePooling/", rev="planned_pickup")
#Pkg.instantiate()   #there may be an error here concerning PyCall. The dependencies assume Python to be installed on your system, with package 'matplotlib' installed.
                    #Either just run 'conda install -c conda-forge matplotlib' in your system shell, or follow instructions below (after running this cell and getting an error) to solve everything from within Julia.
import RidePooling




global name = ARGS[2]
global settings_name = ARGS[1]




include(settings_name)


for (name, path) in paths
    try
	mkpath(path)
    catch e
	println(e.msg)
    end
end

map_folder = "./example_map/map/"
#dispatcher_folder = "./example_map/delays_wt1reldelta1/"


for name in readdir(map_folder, join=false)
	try
	cp(map_folder*"/"*name, paths[:map]*"/"*name)
catch e
println(e)
end
end
#for name in readdir(dispatcher_folder, join=false)
#	try
#	cp(dispatcher_folder*"/"*name, paths[:dispatcher]*"/"*name)
#catch e
#println(e)
#end
#end

for (name, path) in paths
    try
	mkpath(path)
    catch e
	println(e.msg)
    end
end



N=1
SGE_TASK_ID=12
JOB_NAME=name
run(`/usr/ds/bin/julia sim.jl $SGE_TASK_ID $settings_name $JOB_NAME`)
