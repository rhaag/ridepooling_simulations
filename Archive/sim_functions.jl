export data_functions, save_path, getValues
import Pkg
Pkg.activate(".")

using DataFrames
using DelimitedFiles



# Dict of the functions used for evaluation
# QUESTION Calculate Variance for all of these?
efficiency(model) = RP.requested_distance(model) /RP.driven_distance(model)

data_type_functions = Dict(
    :served_percentage => RP.served_percentage,
    :driven_distance => RP.driven_distance,
    :requested_distance => RP.requested_distance,
    :mean_relative_delay => RP.mean_relative_delay,
    :efficiency => efficiency,
    :occupancy => RP.mean_occupancy,
    :mean_relative_delay => RP.mean_relative_delay,
    :mean_relative_waiting_time => RP.mean_relative_waiting_time,
    :served_percentage_no_waiting => RP.served_percentage_no_waiting,
   # :mean_occupancy => RP.mean_occupancy, #BUG In Reading TUples from a CSV File
)




"""
 Converts a given index to a 2D Matrix Index with the given Matrix size
"""
function getValue(index, xmin, xmax, xlen, ymin, ymax, ylen)
    xstep = (xmax-xmin)/xlen
    ystep = (ymax-ymin)/ylen


    x = (index%xlen) * xstep + xmin
    y = trunc(Int64, index/ylen) * ystep + ymin
    return x,y
end

function getX(index, xmin, xmax, xlen)
	xstep = (xmax-xmin)/xlen
	return (index%xlen) * xstep + xmin
end

function getY(index, ymin, ymax, ylen)
	ystep = (ymax-ymin)/ylen
	return trunc(Int64, index/ylen) * ystep + ymin
end

function getLogValue(index, xmin, xmax, xlen::Int64, ymin, ymax, ylen::Int64)
    xstep = (xmax-xmin)/xlen
    ystep = (ymax-ymin)/ylen

    xs = exp.(LinRange(log(xmin), log(xmax), xlen))
    x = xs[(index)%xlen+1]
    y = trunc(Int64, index/ylen) * ystep + ymin
    return x,y
end



function rand_value(lower,upper)
    rand(Float64) *(upper-lower) + lower
end

function rand_value(list)
    rand(list)
end

"""

"""
function getLogLogValue(index, xmin, xmax, xlen::Int64, ymin, ymax, ylen::Int64)


    xs = exp.(LinRange(log(xmin), log(xmax), xlen))
    ys = exp.(LinRange(log(ymin), log(ymax), ylen))
    x = xs[(index)%xlen+1]
    y = ys[trunc(Int64, (index)/ylen)+1]
    return x,y
end

"""
 Function for Running a RidePooling simulation with the normalized Frequency x
"""
function simulate_rp(paths::Dict, N::Int64, specs, params; served = 10*N, requested=10*N)

    println("Get Model with with \nspecs=$specs")
    #Make Model

        if params[:request_type] = :drive_and_wait
            request_type = (;
                params[:request_type],
                cost_idle_weight = params[:weight],
            )
        else if params[:request_type] = :resubmission
            request_type = (;
            params[:request_type],
            resubmission_time_faktor = params[:resubmission_time_factor],
            )
        end
        merge(specs, request_type)




        @time model=RP.get_model(;N_bus=N,ν=x/t0,specs...);
        @time RP.run!(model;requested=requested, served=served)

        data = Dict()
        for (name, param) in params
            data[name] = [param,]
        end
        for (name, func) in data_type_functions
            data[name] = [func(model),]
        end







    open(paths[:data]*"$(lpad(index, 5, "0")).csv", "w") do io
        writedlm(io, Iterators.flatten(([names(data)], eachrow(data))), ';')
    end
    #Save the model for possible later reference
    RP.savemodel(paths[:model]*"$(lpad(index, 5, "0")).model",model;route_matrix=false)
end
