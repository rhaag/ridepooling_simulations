#!/bin/bash

#$ -S /bin/bash
#$ -cwd
#$ -q grotrian.q
#$ -j yes
#$ -N frequency_v_pickup_times
#$ -t 1-1600
#$ -tc 900


SETTINGS_NAME="./standard_settings.jl"

/usr/ds/bin/julia sim.jl $SGE_TASK_ID $SETTINGS_NAME $JOB_NAME
