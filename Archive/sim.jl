using Pkg
Pkg.activate(".")

#Pkg.add(url="https://gitlab.gwdg.de/smuehle1/RidePooling/", rev="planned_pickup")
#Pkg.instantiate()   #there may be an error here concerning PyCall. The dependencies assume Python to be installed on your system, with package 'matplotlib' installed.
                    #Either just run 'conda install -c conda-forge matplotlib' in your system shell, or follow instructions below (after running this cell and getting an error) to solve everything from within Julia.
import RidePooling

#include("../RidePooling/src/RidePooling.jl")
RP = RidePooling
using DataFrames
using CSV








try
    global name = ARGS[3]
    global settings_path = ARGS[2]
    global index=eval(Meta.parse(ARGS[1]))
    println("Taken Command line values:\n\tIndex=$index\n\tsettings_path=$settings_path")
catch e
    print(e)
    global name = "test"
    global settings_path = "./standard_settings.jl"
    global index=1#eval(Meta.parse(ARGS[1]))
    println("Take standard values:\n\tIndex=$index\n\tsettings_path=$settings_path")
end
include(settings_path)
#include("./sim_functions.jl")#

params = Dict()
for (name, bound) in bounds
    params[name] = rand_value(bound...)
end


if !isdir(paths[:data])
    error("Please create data folder at $(paths[:data])")

    return
end
if !isdir(paths[:model])
    error("Please create model folder at $(paths[:model])")
    return
end

map_folder = paths[:map]
include(paths[:map]*"map.jl") #


random_gens = Dict(params[request_type] => [rng_type, params[:dt_earliest_pickup]])
specs=(;
        map=mymap,
        route_matrix=RM,
        subspaces=:all_edges,
        routing=:lookup,
        speed_dict = speed_dict,
        seed = 3,
        random_gens = random_gens,
        t_max_wait = typemax(Float64), #TODO Remove t_max_wait!
        t0 = 1,
        rejection_criterion=((:any_waiting_time,params[:any_waiting_time]), (:any_relative_detour, params[:any_waiting_time])),

)

specs = merge(specs, dispatcher)

@time data = simulate_rp(paths,N, t0, specs, request_type, served=served, requested=requested)
