using Pkg
Pkg.activate(".")
import RidePooling
RP = RidePooling
include("./sim_functions.jl")



request_type=:resubmission                       # request_type that should be simulated
#name="test_1"                                         # Name of the simulation
path="./$name/" #Change"/scratch01.local/rhaag/$(String(request_type))/$name/"  # Path were everything will be saved


# Simulation Settings
N = 1#10 #Number of Busses
requested = 1000#Change6000 * N        # Number of requests, that need to be made
served = 1000#Change6000 * N           # Number of requests, that need to be served

#Random Generator
rng_type = :even

# For mapping the 1D Index to 2D

bounds = Dict()


bounds[:dt_earliest_pickup] = (0, 5)
bounds[:weight] = (0, 4) #TODO Weight still needs to be implemented for top_level usage
bounds[:resubmission] = (0.9, 1.5)
bounds[:request_type] = ([:resubmission, :drive_and_wait],)





# File Organization
paths = Dict(:data => "/results/data/",
            :model => "/results/model/",
            :map => "/example_map/map0/"
            #:dispatcher => "/example_map/delays_wt1reldelta1/"
)


# Functions
#indexing(index) = indexing_method(index-1, xBounds..., im_size[1], yBounds..., im_size[2])

for (name, subdir) in paths
    paths[name] = path * subdir
end
