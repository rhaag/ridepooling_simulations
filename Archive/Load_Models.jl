import Pkg
Pkg.add(url="https://gitlab.gwdg.de/smuehle1/RidePooling/", rev="planned_pickup")
Pkg.instantiate()   #there may be an error here concerning PyCall. The dependencies assume Python to be installed on your system, with package 'matplotlib' installed.
                    #Either just run 'conda install -c conda-forge matplotlib' in your system shell, or follow instructions below (after running this cell and getting an error) to solve everything from within Julia.

using RidePooling
RP = RidePooling
using DataFrames
using CSV

project_folder = "/scratch01.local/rhaag/projects/planned_pickup/frequency_v_pickup_times/"
model_folder = project_folder*"/models/"
output_file = "results.csv"


efficiency(model) = requested_distance(model) /driven_distance(model) 
#TODO quality = 
data_type_functions = Dict(
    :served_percentage => served_percentage,
    :driven_distance => driven_distance,
    :requested_distance => requested_distance,
    :mean_relative_delay => mean_relative_delay,
    :efficiency => efficiency,
    :occupancy => mean_occupancy,
    :mean_relative_delay => mean_relative_delay,
    :mean_relative_waiting_time => mean_relative_waiting_time,
    :served_percentage_no_waiting => served_percentage_no_waiting,
   # :mean_occupancy => RP.mean_occupancy, #BUG In Reading TUples from a CSV File
)


model_filenames = readdir(model_folder, join=false)


model = RP.loadmodel(model_folder*model_filenames[1])
data = Dict()
data[:frequency] = model.ν
data[:resubmission] = model.random_gens[:resubmission][2]
for (name, func) in data_type_functions
    data[name] = func(model)
end
df = DataFrame(data)



for name in model_filenames[2:end]
    model = RP.loadmodel(model_folder*name)
    data = Dict()
    data[:frequency] = model.ν
    data[:resubmission] = model.random_gens[:resubmission][2]
    for (name, func) in data_type_functions
        data[name] = func(model)
    end
    data = DataFrame(data)
    append!(df, data)
end

println("Saving Combined Dataframe to $output_file")
CSV.write(output_file, df)
