using DataFrames
# Everything in the following will be 

dispatchers_ν_80 = DataFrame([
    (map= (:star_grid_map,(32,32)), N_bus = 10, cost = :combined_delays,       rejection_criterion = ((:any_waiting_time, 0.35), (:any_relative_detour, 1.)),   	ν= 10.628969520905192),
    (map= (:star_grid_map,(32,32)), N_bus = 10, cost = :combined_delays,       rejection_criterion = ((:any_waiting_time, 0.5), (:any_relative_detour, 0.5)),   	ν= 16.452499904510834),
    (map= (:star_grid_map,(32,32)), N_bus = 10, cost = :combined_delays,       rejection_criterion = ((:any_waiting_time, 1.), (:any_relative_detour, 0.5)),   	ν= 17.72802734375),
    (map= (:star_grid_map,(32,32)), N_bus = 10, cost = :combined_delays,       rejection_criterion = ((:any_waiting_time, 1.), (:any_relative_detour, 1.)),    	ν = 20.4378465809477),
    #(map= (:star_grid_map,(32,32)), N_bus = 10, cost = :combined_trajectory,   rejection_criterion = ((:any_relative_delay, 2),),                              	ν = 18.517258102635545),
    (map= (:star_grid_map,(32,32)), N_bus = 10, cost = :combined_trajectory,   rejection_criterion = ((:any_waiting_time, 1.,), (:any_relative_detour, 0.5)),  	ν = 18.113527765908774),
    (map= (:star_grid_map,(32,32)), N_bus = 10, cost = :combined_trajectory,   rejection_criterion = ((:any_waiting_time, 1.,), (:any_relative_detour, 1.)),  	ν = 20.638684131196953),
    (map= (:star_grid_map,(32,32)), N_bus = 10, cost = :combined_trajectory,   rejection_criterion = ((:any_waiting_time, 0.5), (:any_relative_detour, 1.)),    	ν = 16.27171184417472),
    (map= (:star_grid_map,(32,32)), N_bus = 10, cost = :combined_trajectory,   rejection_criterion = ((:any_waiting_time, 0.35), (:any_relative_detour, 1.)),    	ν = 10.325502712039059)
    ])

