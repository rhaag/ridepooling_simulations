using Pkg
Pkg.activate(".")
Pkg.instantiate()


#Pkg.add(url="https://gitlab.gwdg.de/smuehle1/RidePooling/", rev="myopic_earliest_pickup")
Pkg.add(url="https://gitlab.gwdg.de/ruben.haag/ridepooling_eval.git", rev="main")

Pkg.resolve()



if length(ARGS) == 3
    name = ARGS[3]
    settings_path = ARGS[2]
    index=eval(Meta.parse(ARGS[1]))
    println("Taken Command line values:\n\tIndex=$index\n\tsettings_path=$settings_path")
else
    throw("Value Error: Please give the command line arguments in the following order\nname\nsettings_path\nindex")
end

include(settings_path)

savePath = savePath * "$(String(request_type))/$cost/$name/"
modelPath = savePath * modelPath

try
	mkpath(savePath)
catch e
	println(e.msg)
end


try
	mkpath(modelPath)
catch e
	println(e.msg)
end


#Test, if everything runs
run(`julia sim.jl $index $settings_path $name`)
