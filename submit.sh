#!/bin/bash

#$ -S /bin/bash
#$ -cwd
#$ -q grotrian.q
#$ -j yes
#$ -N anywt035_anydt01
#$ -t 1-1600
#$ -tc 400


SETTINGS_NAME="./settings.jl"

#Run simulation
julia sim.jl $SGE_TASK_ID $SETTINGS_NAME $JOB_NAME
