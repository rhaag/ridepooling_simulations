max_waiting_time=t0
max_relative_detour=1.0

dispatcher=(;
        cost=:trajectory_length,
        rejection_criterion=((:any_relative_detour,max_relative_detour),(:any_waiting_time,max_waiting_time))
        )
