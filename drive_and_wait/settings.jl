

savePath = "/scratch01.local/rhaag/comparison/"
mapPath = "./Maps/"
dispatcherPath = "./Dispatchers/"
modelPath = "/models/"

N = 10
seed = 1
map = (:star_grid_map, (32,32))
cost = :combined_delays
rejection = ((:any_waiting_time, 0.35), (:any_relative_detour, 1.))

# Only Limit the number of requests to ensure, that all system get exactly the same requests
requested = 10000 * N

# Random Distribution of dt_earliest_pickup
rng_type = :even
request_type = :drive_and_wait

xlen = 80
ylen = 40
########## Paretho Parameters
cost_idle_weight_range = (0.1, 10, ylen) #TODO Logarithmisch
resubmission_time_factor_range = (0.5, 1.5, ylen)

#Range for dt_ealiest_pickup
dt_earliest_pickup_range = (0, 3., xlen)


#= Values I want to calculate for every model
req_quantitys_mean = [
	:relative_delay,
	:delay,
	:relative_waiting_time,
	:waiting_time,
	:served,
	:requested_distance,
	:requested_time,
	:total_time,
	:travelling_time,
    :resubmitted_served_percentage,
	:dt_earliest_served,
    :dt_earliest,
	:relative_dt_earliest
]

req_quantitys_sum = [
	:count_req,
	:resubmitted_all,
	:resubmitted_served,
]

bus_quantitys = [
	:cooldown_time,
]

model_quantitys = [
	:driven_distance,
	:efficiency,
	:quality,
	:model_time
]=#
