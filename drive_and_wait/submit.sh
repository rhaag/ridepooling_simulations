#!/bin/bash

#$ -S /bin/bash
#$ -cwd
#$ -q grotrian.q
#$ -j yes
#$ -N anywt035_anydt01
#$ -t 1-3200
#$ -tc 400


SETTINGS_NAME="./drive_and_wait/settings.jl"
#Change into the Folder, where everything else is
cd ..

#Run simulation
julia sim.jl $SGE_TASK_ID $SETTINGS_NAME $JOB_NAME
